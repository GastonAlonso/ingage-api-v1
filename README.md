Ingage Web API v1
=================

The API for Ingage Web.

### Development

1\. Clone this repository

```shell
git clone https://bitbucket.org/scrollmotiongit/ingage-api-v1.git

cd ingage-api-v1
```

2\. Build the image

```shell
docker build -f dockerfile-dev -t scrollmotion/ingage-api-v1:dev .
```

3\. Run the image

```shell
# run as built
docker run -it -p 8083:80 scrollmotion/ingage-api-v1:dev

# run with live updating of current directory
docker run -it -p 8083:80 -v `pwd`:/opt/app -v /opt/app/node_modules scrollmotion/ingage-api-v1:dev
```

4\. Browse to [localhost:8083](http://localhost:8083) to access the API

### File Structure

```
ingage-api-v1/
├── components/          (leverage components)
│   ├── http/            (http components)
│   │   ├── get_index.js (get requests to '/')
│   │   ├── get_stub.js  (get requests to '/:stub')
│
├── lib/                 (library files)
│   ├── adapter/         (CS4 adapter)
│   ├── flatten.mjs      (array flattening helper)
│   ├── http.mjs         (http request helper)
│   ├── logger.mjs       (logging helper)
│
├── .dockerignore        (ensure docker ignores certain files)
├── .eslintrc.js         (linting configuration)
├── .gitignore           (ensure git ignores certain files)
├── Dockerfile.dev       (development dockerfile)
├── index.js             (entrypoint)
├── package-lock.json    (npm dependency lock)
├── package.json         (npm package manifest)
├── README.md            (this file)
├── server.mjs           (server entrypoint)
```
