import path from 'path'
import debug from 'debug'
import { manager } from 'leverage-js'
import http from 'leverage-plugin-http'

const debugServer = debug('[api][v1][server]')

// @TODO: Decide whether this should be manually toggled
// process.env.DEBUG = '[api]*'

const services = path.resolve('.', 'services')
const components = path.resolve('.', 'components', 'http')

debugServer('Loading plugins, services, and components...')

manager.add(http, services, components)

debugServer('Finished loading plugins, services, and components.')

debugServer('Starting http server...')

http.listen(80)
