export default text => {
    // walk the string backwards looking for first space
    for (let i = text.length - 1; i > 0; i--) {
        if (text[i] === ' ') {
            // join the text before the space and the text after using
            //  a non-breaking space character.
            // \u00a0 -> &nbsp;
            return `${text.substr(0, i)}\u00a0${text.substr(i + 1)}`;
        }
    }

    return text;
};
