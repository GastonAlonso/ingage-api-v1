import request from 'request'

const http = (...args) =>
    new Promise((resolve, reject) => {
        request(...args, (err, response, body) => {
            if (err) reject(err)
            else resolve({ body, response })
        })
    })

export default http
