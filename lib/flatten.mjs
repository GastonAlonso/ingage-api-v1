export default xs =>
    xs.reduce((ys, x) => ys.concat(x), [])
