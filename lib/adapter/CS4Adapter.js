const CS4Story = require('./CS4Adapter/CS4Story');
const CS4Video = require('./CS4Adapter/CS4Video');
const CS4TextAndImage = require('./CS4Adapter/CS4TextAndImage');
const CS4Flipbook = require('./CS4Adapter/CS4Flipbook');
const CS4PointsOfInterest = require('./CS4Adapter/CS4PointsOfInterest');
const CS4Compare = require('./CS4Adapter/CS4Compare');
const CS4Impact = require('./CS4Adapter/CS4Impact');
const CS4Email = require('./CS4Adapter/CS4Email');
const CS4Deck = require('./CS4Adapter/CS4Deck');
const CS4RowsAndColumns = require('./CS4Adapter/CS4RowsAndColumns');
const CS4Gallery = require('./CS4Adapter/CS4Gallery');

const translators = [
    CS4Video,
    CS4TextAndImage,
    CS4Flipbook,
    CS4PointsOfInterest,
    CS4Compare,
    CS4Impact,
    CS4Email,
    CS4Story,
    CS4Deck,
    CS4RowsAndColumns,
    CS4Gallery,
];

class CS4Adapter {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
    }

    getTranslator () {
        const { _cs4, _editor } = this;

        for (const Translator of translators) {
            if (Translator.understands(_cs4, _editor)) return Translator;
        }

        return false;
    }

    toStub () {
        const { _cs4, _editor } = this;
        const Translator = this.getTranslator(_cs4);

        if (!Translator) {
            return false;
        }

        return new Translator(_cs4, _editor, CS4Adapter).toStub();
    }

    static understands (cs4, editor) {
        for (const Translator of translators) {
            if (Translator.understands(cs4, editor)) return true;
        }

        return false;
    }
}

module.exports = CS4Adapter;
