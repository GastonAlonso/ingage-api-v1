class CS4Gallery {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._toStub = this.buildStub();
    }

    buildStub () {
        const { _cs4 } = this;

        let data = null;
        try {
            data = JSON.parse(_cs4.overlays[0].properties.pageModelData);
        } catch (e) {
            console.log('[API][Adapter] Error parsing CS4: ', e);
            return false;
        }

        // TODO: Remove when we add custom fonts
        if (_cs4.globalFontOverride) {
            data.headingText.fontFamily = _cs4.globalFontOverride;
            data.bodyText.fontFamily = _cs4.globalFontOverride;
        }

        return data;
    }

    toStub () {
        return this._toStub;
    }

    static understands (cs4) {
        if (
            !(
                cs4.overlays &&
                cs4.overlays.length > 0 &&
                cs4.overlays[0].properties
            )
        )
            return false;

        return cs4.overlays[0].properties.pageType === 'gallery';
    }
}

module.exports = CS4Gallery;
