import removeWidowWords from '../../removeWidowWords';

import { getSizeFromIOS } from './universal-text';

class CS4Email {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._toStub = this.buildStub();
    }

    mapLayout (layout) {
        const layoutMapping = {
            // Centered Text
            'email-text-centered': 1,
            // Left Aligned Text
            'email-text-left-half': 2,
            // Left Aligned, Bold Text
            'email-text-left-third': 3,
        };

        return layoutMapping[layout] || 2;
    }

    mapTheme (theme) {
        const themeMapping = {
            light: 1,
            dark: 2,
        };

        return themeMapping[theme] || 2;
    }

    buildStub () {
        const { _cs4, _editor } = this;
        const { id } = _cs4;
        const editorPage = _editor.pages[id];

        let meta = '';

        if (editorPage.layoutId) {
            meta = editorPage.layoutId.split('|');
        }

        const layout = this.mapLayout(meta[0]);
        const theme = this.mapTheme(meta[1]);

        let title = null;
        let body = null;

        let titleStyle = null;
        let bodyStyle = null;

        let background = null;
        let button = null;
        let type = null;
        // eslint-disable-next-line camelcase
        let subscribe_url_long = null;

        let width = null;
        let height = null;

        for (const overlay of _cs4.overlays) {
            if (overlay.editorId === 'EmailPageImageContainer') {
                background =
                    overlay.overlays[0].transformedImage ||
                    overlay.overlays[0].image;

                width = overlay.overlays[0].width;
                height = overlay.overlays[0].height;
            } else if (overlay.editorId === 'EmailPageHeader') {
                title = overlay.text;
                titleStyle = {
                    name: overlay.textStyleId || 'title-light',
                    size: overlay.textSize
                        ? overlay.textSize.replace('px', '.0')
                        : '32.0',
                };
            } else if (overlay.editorId === 'EmailPageBody') {
                body = removeWidowWords(overlay.text);
                bodyStyle = {
                    name: overlay.textStyleId || 'body',
                    size: overlay.textSize || '20.0',
                };
            } else if (overlay.editorId === 'EmailPageSubmitButton') {
                button = overlay.text;

                // not configured
                if (!overlay.onTouchUpInside[0]) {
                    type = '';
                    // eslint-disable-next-line camelcase
                    subscribe_url_long = '';
                } else {
                    type = overlay.onTouchUpInside[0].emailType || '';
                    // eslint-disable-next-line camelcase
                    subscribe_url_long =
                        overlay.onTouchUpInside[0].subscribe_url_long || '';
                }
            }
        }

        if (width.includes('%')) {
            width = 768;
            height = 384;
        } else {
            width = parseInt(width.replace('px', ''));
            height = parseInt(height.replace('px', ''));
        }

        return {
            type: 'email-page',
            version: 1.0,
            layout,
            theme,
            background,
            button,
            service: {
                type,
                // eslint-disable-next-line camelcase
                subscribe_url_long,
            },
            title: {
                content: title,
                style: titleStyle.name,
                size: getSizeFromIOS(titleStyle),
                // TODO: Remove when we add custom fonts
                fontFamily: _cs4.globalFontOverride || 'benton-sans',
            },
            body: {
                content: body,
                style: bodyStyle.name,
                size: getSizeFromIOS(bodyStyle),
                // TODO: Remove when we add custom fonts
                fontFamily: _cs4.globalFontOverride || 'benton-sans',
            },
            _meta: {
                width,
                height,
            },
        };
    }

    toStub () {
        return this._toStub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        // if there is no editorPage, this is not an email
        if (!editorPage) return false;

        return editorPage.templateType === 'Email';
    }
}

module.exports = CS4Email;
