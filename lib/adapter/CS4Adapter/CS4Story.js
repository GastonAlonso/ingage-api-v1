import { getSizeFromIOS } from './universal-text';

class CS4Story {
    constructor (cs4, editor, CS4Adapter) {
        this._cs4 = cs4;
        this._editor = editor;
        this.CS4Adapter = CS4Adapter;

        this._stub = this.buildStub();
    }

    mapLayout (layout) {
        const layoutMapping = {
            'cover-text-left-half': 1,
            left2: 1,
            'cover-text-left-third': 2,
            left1: 2,
            'cover-text-top': 3,
            top1: 3,
            'cover-text-bottom-thick': 4,
            bottom1: 4,
            'cover-text-center': 5,
            center1: 5,
            'cover-text-bottom-thin': 6,
            bottom2: 6,
            'cover-full-page': 7,
        };

        return layoutMapping[layout] || 1;
    }

    mapCoverLayout (layout) {
        const layoutCoverMapping = {
            1: 'verticalSpreadLarge',
            2: 'verticalSpreadSmall',
            3: 'horizontalSpread',
            4: 'horizontalSpread',
            5: 'horizontalSpread',
            6: 'horizontalSpread',
            7: 'hidden',
        };

        return layoutCoverMapping[layout] || 'verticalSpreadLarge';
    }

    mapPosition (layout) {
        const positionMapping = {
            1: { x: 0, y: 0 },
            2: { x: 0, y: 0 },
            3: { x: 0, y: 0.052 },
            4: { x: 0, y: 0.637 },
            5: { x: 0, y: 0.3535 },
            6: { x: 0, y: 0.637 },
            7: { x: 0, y: 0 },
        };

        return positionMapping[layout] || { x: 0, y: 0 };
    }

    mapTitleStyle (layout) {
        const titleStyleMapping = {
            1: {
                weight: 'light',
                size: 'extra-extra-large',
                align: 'left',
            },
            2: {
                weight: 'bold',
                size: 'extra-large',
                align: 'center',
            },
            3: {
                weight: 'light',
                size: 'extra-extra-large',
                align: 'center',
            },
            4: {
                weight: 'bold',
                size: 'extra-large',
                align: 'center',
            },
            5: {
                weight: 'bold',
                size: 'medium',
                align: 'center',
            },
            6: {
                weight: 'light',
                size: 'large',
                align: 'left',
            },
            7: {
                weight: 'light',
                size: 'large',
                align: 'left',
            },
        };

        return titleStyleMapping[layout] || titleStyleMapping[1];
    }

    mapSubtitleStyle (layout) {
        return {
            weight: 'light',
            size: 'medium',
            align: layout == 1 || layout == 6 ? 'left' : 'center',
        };
    }

    buildStub () {
        const homePageStub = this.buildHomePageStub();
        const pageSetIds = this.getPageSetIds();

        const pageSetStubs = [];

        for (const pageSetId of pageSetIds) {
            // don't add hidden sections
            if (this.isPageSetHidden(pageSetId)) continue;

            pageSetStubs.push(this.buildPageSetStub(pageSetId));
        }

        const displayName =
            this._cs4.displayName === 'Untitled Story'
                ? ''
                : this._cs4.displayName;

        const stub = Object.assign(
            {},
            {
                type: 'story',
                version: 1,
                title: displayName,
                cover: homePageStub,
                sections: pageSetStubs,
                logo: this._editor.logo,
            },
        );

        return stub;
    }

    buildPageSetStub (pageSetId) {
        const { _editor } = this;
        const pageSetName = this.getPageSetName(pageSetId);
        const pageSetPages = this.getPageSetPages(pageSetId);

        const pages = [];

        for (const page in pageSetPages) {
            // don't add hidden pages
            if (pageSetPages[page].hidden) continue;

            if (!this.pageExistsInEditor(pageSetPages[page])) continue;

            // TODO: Remove when we add custom fonts
            if (this._cs4.globalFontOverride) {
                pageSetPages[
                    page
                ].globalFontOverride = this._cs4.globalFontOverride;
            }

            pages.push(
                new this.CS4Adapter(pageSetPages[page], _editor).toStub(),
            );
        }

        return {
            name: pageSetName,
            pages,
        };
    }

    buildHomePageStub () {
        const { _cs4, _editor } = this;

        // no home page
        if (!_cs4.pageSets.home || _cs4.pageSets.home.pages.length === 0) {
            return null;
        }

        let stub = null;

        if (
            _cs4.pageSets.home.pages[0].overlays[0].properties &&
            _cs4.pageSets.home.pages[0].overlays[0].properties.pageModelData
        ) {
            stub = JSON.parse(
                _cs4.pageSets.home.pages[0].overlays[0].properties
                    .pageModelData,
            );
        }

        // no stub, build it from content spec
        if (!stub) {
            const meta = _editor.homepage.layoutId.split('|');
            const layout = this.mapLayout(meta[0]);
            const theme = meta[1] || 'light';
            const position = this.mapPosition(layout);

            let title = null;
            let subtitle = null;
            let background = null;

            let titleStyle = null;
            let subtitleStyle = null;

            for (const overlay of _cs4.pageSets.home.pages[0].overlays) {
                switch (overlay.id) {
                    case 'HomePageTitle':
                        title = overlay.text;
                        titleStyle = this.mapTitleStyle(layout);
                        break;

                    case 'HomePageSubtitle':
                        subtitle = overlay.text;
                        subtitleStyle = this.mapSubtitleStyle(layout);
                        break;

                    case 'HomePageImageContainer':
                        background =
                            overlay.overlays[0].transformedImage ||
                            overlay.overlays[0].image ||
                            overlay.overlays[0].video;

                        break;

                    default:
                        break;
                }
            }

            const textColor = theme === 'light' ? '#000000' : '#ffffff';

            stub = {
                type: 'cover-page',
                version: 1,
                layout: this.mapCoverLayout(layout),
                dynamicBox: {
                    type: 'dynamic-box-component',
                    version: 1,
                    blurStyle: theme,
                    ...position,
                },
                buttonOverlay: {
                    type: 'overlay-component',
                    version: 1,
                    items: [],
                },
                subheadingText: {
                    type: 'text-component',
                    version: 3,
                    text: title,
                    fontSize: titleStyle.size,
                    fontFamily: 'benton-sans',
                    fontWeight: titleStyle.weight,
                    textAlignment: titleStyle.align,
                    textColor,
                    fontStyle: 'title',
                },
                bodyText: {
                    type: 'text-component',
                    version: 3,
                    text: subtitle,
                    fontSize: subtitleStyle.size,
                    fontFamily: 'benton-sans',
                    fontWeight: titleStyle.weight,
                    textAlignment: subtitleStyle.align,
                    textColor,
                    fontStyle: 'body',
                },
                backgroundMedia: {
                    type: 'media-component',
                    version: 1,
                    mediaType: {},
                },
            };

            if (
                _cs4.pageSets.home.pages[0].overlays[0].overlays[0]
                    .transformedImage ||
                _cs4.pageSets.home.pages[0].overlays[0].overlays[0].image
            ) {
                stub.backgroundMedia.mediaType.image = {
                    type: 'image-component',
                    version: 2,
                    foregroundBlurStyle: 'light',
                    filename: background,
                    backgroundType: {
                        type: 'media-background-type',
                        version: '1.0',
                        hexColor: '#FFFFFF',
                        style: 'blur',
                        blurStyle: 'dark',
                    },
                    transformation: {
                        x: 0,
                        y: 0,
                        scale: 1,
                        rotation: 0,
                    },
                };
            } else {
                stub.backgroundMedia.mediaType.video = {
                    type: 'video-component',
                    version: 1,
                    foregroundBlurStyle: 'light',
                    videoFilename: background,
                    backgroundType: {
                        type: 'media-background-type',
                        version: '1.0',
                        hexColor: '#FFFFFF',
                        style: 'blur',
                        blurStyle: 'dark',
                    },
                    trimRange: {
                        endTime: 0,
                        startTime: 0,
                    },
                    transformation: {
                        x: 0,
                        y: 0,
                        scale: 1,
                        rotation: 0,
                    },
                    playbackOptions: {
                        playsOnPage: true,
                        hasMuteButton: false,
                        hasFullScreenButton: false,
                    },
                };
            }
        }

        return stub;
    }

    isPageSetHidden (pageSetId) {
        const { _cs4 } = this;

        return !_cs4.pageSets[pageSetId] || _cs4.pageSets[pageSetId].hidden;
    }

    getPageIds () {
        const { _editor = { pages: {} } } = this;

        return [
            ...Object.keys(_editor.pages),
            'homepage',
        ];
    }

    getPageSetIds () {
        const { _editor = { pageSetsOrder: [] } } = this;

        return _editor.pageSetsOrder;
    }

    getPageSetName (pageSetId) {
        const { _cs4 } = this;

        return _cs4.pageSets[pageSetId].displayName;
    }

    getPageSetPages (pageSetId) {
        const { _cs4 } = this;

        return _cs4.pageSets[pageSetId].pages;
    }

    pageExistsInEditor ({ id }) {
        const pageIds = this.getPageIds();

        return pageIds.indexOf(id) > -1;
    }

    toStub () {
        return this._stub;
    }

    static understands (cs4) {
        return Boolean(cs4.pageSets);
    }
}

module.exports = CS4Story;
