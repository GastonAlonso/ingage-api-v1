import removeWidowWords from '../../removeWidowWords';

import { getSizeFromIOS } from './universal-text';

class CS4PointsOfInterest {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._stub = this.buildStub();
    }

    mapLayout (layout) {
        const layoutMapping = {
            'poi-full-page': 'full',
            'poi-text-horizontal-bottom-third': 'horizontal',
            'poi-text-vertical-right-half': 'vertical-halves',
            'poi-text-vertical-right-third': 'vertical-thirds',
            'vertical-thirds': 'vertical-thirds',
            'poi-text-squared': 'square',
        };

        return layoutMapping[layout] || 1;
    }

    mapTheme (theme) {
        const themeMapping = {
            white: 1,
            'light-gradient': 2,
            'medium-gradient': 3,
            'dark-gradient': 4,
        };

        return themeMapping[theme] || 1;
    }

    buildStub () {
        const { _cs4, _editor } = this;

        const { id } = _cs4;
        const editorPage = _editor.pages[id];

        const layout = this.mapLayout(editorPage.layoutId);
        // todo: fill in
        const theme = 0;
        const title = _cs4.overlays[0].text;
        const body = removeWidowWords(_cs4.overlays[1].text);

        const titleStyle = {
            name: _cs4.overlays[0].textStyleId || 'title-bold',
            size: _cs4.overlays[0].textSize || '32.0',
        };
        const bodyStyle = {
            name: _cs4.overlays[1].textStyleId || 'body',
            size: _cs4.overlays[1].textSize || '20.0',
        };

        const background =
            _cs4.overlays[2].overlays[0].transformedImage ||
            _cs4.overlays[2].overlays[0].image;

        const points = this.buildPoints();

        const image = _cs4.overlays[2].overlays[0];

        let width = parseInt(image.width.replace('px', ''), 10);
        let height = parseInt(image.height.replace('px', ''), 10);

        return {
            type: 'points-of-interest-page',
            version: '1.0',
            layout,
            theme,
            background,
            points,
            title: {
                content: title,
                style: titleStyle.name,
                size: getSizeFromIOS(titleStyle),
                // TODO: Remove when we add custom fonts
                fontFamily: _cs4.globalFontOverride || 'benton-sans',
            },
            body: {
                content: body,
                style: bodyStyle.name,
                size: getSizeFromIOS(bodyStyle),
                // TODO: Remove when we add custom fonts
                fontFamily: _cs4.globalFontOverride || 'benton-sans',
            },
            _meta: {
                width,
                height,
            },
        };
    }

    buildPoints () {
        const { _cs4, _editor } = this;

        const { id } = _cs4;
        const editorPage = _editor.pages[id];

        const points = [];

        for (const point of editorPage.sections.section1.points) {
            points.push(this.buildPoint(point));
        }

        return points;
    }

    isCorrectSpawnAction (spawnAction) {
        return Array.isArray(spawnAction.data.overlays);
    }

    buildPoint (point) {
        const { _cs4 } = this;
        const { type, buttonId } = point;
        const properties = {};

        switch (type) {
            case 'blurb':
                for (const overlay of point.spawnActions[0].data.overlays) {
                    if (overlay.type === 'label') {
                        const { text } = overlay;

                        Object.assign(properties, { text });
                    }
                }
                break;

            case 'image': {
                for (const spawnAction of point.spawnActions) {
                    // skip if wrong spawnAction
                    if (!this.isCorrectSpawnAction(spawnAction)) continue;

                    for (const overlay of spawnAction.data.overlays[0]
                        .overlays) {
                        if (overlay.type === 'label') {
                            const { text } = overlay;

                            Object.assign(properties, { text });
                        } else if (overlay.type === 'container') {
                            const image =
                                overlay.overlays[0].transformedImage ||
                                overlay.overlays[0].image;

                            Object.assign(properties, { image });
                        }
                    }
                }
                break;
            }

            case 'video': {
                for (const spawnAction of point.spawnActions) {
                    // skip if wrong spawnAction
                    if (!this.isCorrectSpawnAction(spawnAction)) continue;

                    for (const overlay of spawnAction.data.overlays[0]
                        .overlays) {
                        if (overlay.displayName === 'poiVideoTitleLabel') {
                            const title = overlay.text;

                            Object.assign(properties, { title });
                        } else if (
                            overlay.displayName === 'poiVideoSubtitleLabel'
                        ) {
                            const body = overlay.text;

                            Object.assign(properties, { body });
                        } else if (overlay.displayName === 'poiVideo') {
                            const { video } = overlay;

                            Object.assign(properties, { video });
                        }
                    }
                }
                break;
            }

            case 'audio': {
                for (const spawnAction of point.spawnActions) {
                    // skip if wrong spawnAction
                    if (!this.isCorrectSpawnAction(spawnAction)) continue;

                    for (const overlay of spawnAction.data.overlays[0]
                        .overlays) {
                        if (overlay.type === 'audio') {
                            const { audio } = overlay;

                            Object.assign(properties, { audio });
                        }
                    }
                }
                break;
            }

            case 'map': {
                for (const spawnAction of point.spawnActions) {
                    // skip if wrong spawnAction
                    if (!this.isCorrectSpawnAction(spawnAction)) continue;

                    for (const overlay of spawnAction.data.overlays[0]
                        .overlays) {
                        if (overlay.type === 'utilityButton') {
                            const { data } = overlay;

                            Object.assign(properties, { data });
                        }
                    }
                }

                break;
            }

            case 'website': {
                for (const spawnAction of point.spawnActions) {
                    // skip if wrong spawnAction
                    if (!this.isCorrectSpawnAction(spawnAction)) continue;

                    for (const overlay of spawnAction.data.overlays[0]
                        .overlays) {
                        if (overlay.type === 'utilityButton') {
                            const { data } = overlay;

                            Object.assign(properties, { url: data });
                        }
                    }
                }

                break;
            }

            case 'email': {
                for (const spawnAction of point.spawnActions) {
                    // skip if wrong spawnAction
                    if (!this.isCorrectSpawnAction(spawnAction)) continue;

                    for (const overlay of spawnAction.data.overlays[0]
                        .overlays) {
                        if (overlay.type === 'utilityButton' && overlay.data) {
                            const data = JSON.parse(overlay.data);
                            const { email, subject, body } = data;

                            Object.assign(properties, { email, subject, body });
                        }
                    }
                }

                break;
            }

            default:
                break;
        }

        for (const overlay of _cs4.overlays[2].overlays) {
            if (overlay.editorId === buttonId) {
                const { x, y } = overlay;

                Object.assign(properties, { x, y });

                for (const innerOverlay of overlay.overlays) {
                    if (innerOverlay.type === 'label') {
                        const label = innerOverlay.text;

                        Object.assign(properties, { label });
                    }
                }
            }
        }

        return {
            type,
            properties,
        };
    }

    toStub () {
        return this._stub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        // if there is no editorPage, this is not an email
        if (!editorPage) return false;

        return editorPage.templateType === 'POI';
    }
}

module.exports = CS4PointsOfInterest;
