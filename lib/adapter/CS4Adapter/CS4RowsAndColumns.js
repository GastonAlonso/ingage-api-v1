class CS4Ingage {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._stub = this.buildStub();
    }

    buildStub () {
        const { _cs4 } = this;

        let data = null;
        try {
            data = JSON.parse(_cs4.overlays[0].properties.pageModelData);
        } catch (e) {
            console.log('[API][Adapter] Error parsing CS4: ', e);
            return false;
        }

        // TODO: Remove when we add custom fonts
        if (_cs4.globalFontOverride) {
            data.subheadingText.fontFamily = _cs4.globalFontOverride;
            data.bodyText.fontFamily = _cs4.globalFontOverride;

            if (data.partitions && data.partitions.length) {
                for (partition in data.partitions) {
                    partition.subheading.fontFamily = _cs4.globalFontOverride;
                    partition.body.fontFamily = _cs4.globalFontOverride;
                }
            }
        }

        return data;
    }

    toStub () {
        return this._stub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        // if there is no editorPage, this is not a compare
        if (!editorPage) return false;

        // if there is no overlay, the page isn't supported
        if (!cs4.overlays || !cs4.overlays.length > 0) return false;

        // if there is no properties object, the page isn't supported
        if (!cs4.overlays[0].properties) return false;

        return cs4.overlays[0].properties.pageType === 'rowsAndColumns';
    }
}

module.exports = CS4Ingage;
