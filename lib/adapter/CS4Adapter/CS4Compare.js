import removeWidowWords from '../../removeWidowWords';

import { getSizeFromIOS } from './universal-text';

class CS4Compare {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._stub = this.buildStub();
    }

    mapLayout (layout) {
        const layoutMapping = {
            'compare-full-page': 'full',
            'full-page': 'full',
            'compare-text-horizontal-bottom-third': 'horizontal',
            'horizontal-thirds': 'horizontal',
            'compare-horizontal-thirds': 'horizontal',
            'compare-text-vertical-right-half': 'vertical-halves',
            'vertical-halves': 'vertical-halves',
            'compare-text-vertical-right-third': 'vertical-thirds',
            'vertical-thirds': 'vertical-thirds',
            'compare-text-squared': 'square',
        };

        return layoutMapping[layout] || 1;
    }

    mapTheme (theme) {
        const themeMapping = {
            white: 1,
            'light-gradient': 2,
            'medium-gradient': 3,
            'dark-gradient': 4,
        };

        return themeMapping[theme] || 1;
    }

    mapSlide (slide) {
        const slideMapping = {
            'vertical-drag-bar-white': 'vertical',
            'horizontal-drag-bar-white': 'horizontal',
        };

        return slideMapping[slide] || 'horizontal';
    }

    buildStub () {
        const { _cs4, _editor } = this;
        const { id } = _cs4;

        let stub = null;

        if (
            _cs4.overlays[0].properties &&
            _cs4.overlays[0].properties.pageModelData
        ) {
            stub = JSON.parse(_cs4.overlays[0].properties.pageModelData);
        }

        if (!stub) {
            const editorPage = _editor.pages[id];
            const meta = editorPage.layoutId.split('|');

            // default layout positions for compare scroller
            const positions = [
                0.5,
                0.5,
                0.5,
                0.5,
            ];

            const layout = this.mapLayout(meta[0]);
            const theme = 1;

            const title = _cs4.overlays[1].text;
            const body = removeWidowWords(_cs4.overlays[2].text);

            const titleStyle = {
                name: _cs4.overlays[1].textStyleId || 'title-bold',
                size: _cs4.overlays[1].textSize || '32.0',
            };
            const bodyStyle = {
                name: _cs4.overlays[2].textStyleId || 'body',
                size: _cs4.overlays[1].textSize || '20.0',
            };

            // get position and direction of the slider from contentSpec or use default
            const slidePosition =
                Math.round(_cs4.overlays[0].slider * 100) / 100 ||
                positions[layout - 1];
            const slideDirection = this.mapSlide(meta[1]);

            const compare = [
                _cs4.overlays[0].overlays[0].overlays[0].transformedImage ||
                    _cs4.overlays[0].overlays[0].overlays[0].image,
                _cs4.overlays[0].overlays[1].overlays[0].transformedImage ||
                    _cs4.overlays[0].overlays[1].overlays[0].image,
            ];

            let image = _cs4.overlays[0].overlays[0].overlays[0];

            let width = image.width;
            let height = image.height;

            if (width.includes('%')) {
                width = 768;
                height = 384;
            } else {
                width = parseInt(width.replace('px', ''));
                height = parseInt(height.replace('px', ''));
            }

            stub = {
                type: 'compare-page',
                version: 1,
                layout,
                headingText: {
                    type: 'text-component',
                    version: 2,
                    text: title,
                    fontSize: getSizeFromIOS(titleStyle),
                    fontFamily: _cs4.globalFontOverride || 'benton-sans',
                    fontWeight: 'bold',
                    fontStyle: 'title',
                    textAlignment: 'center',
                    textColor: '#000000',
                },
                bodyText: {
                    type: 'text-component',
                    version: 2,
                    text: body,
                    fontSize: getSizeFromIOS(bodyStyle),
                    fontFamily: _cs4.globalFontOverride || 'benton-sans',
                    fontWeight: 'regular',
                    fontStyle: 'body',
                    textAlignment: 'center',
                    textColor: '#000000',
                },
                compare: {
                    orientation: slideDirection,
                    content: {
                        firstMedia: {
                            type: 'media-component',
                            version: 1,
                            mediaType: {
                                image: {
                                    type: 'image-component',
                                    version: 4,
                                    foregroundBlurStyle: 'light',
                                    filename: compare[0],
                                    backgroundType: {
                                        hexColor: '#FFFFFF',
                                        style: 'blur',
                                    },
                                    transformation: {
                                        x: 0,
                                        y: 0,
                                        scale: 1,
                                        rotation: 0,
                                    },
                                },
                            },
                        },
                        firstButtonOverlay: {
                            type: 'overlay-component',
                            version: 2,
                            items: [],
                        },
                        secondMedia: {
                            type: 'media-component',
                            version: 1,
                            mediaType: {
                                image: {
                                    type: 'image-component',
                                    version: 4,
                                    foregroundBlurStyle: 'light',
                                    filename: compare[1],
                                    backgroundType: {
                                        hexColor: '#FFFFFF',
                                        style: 'blur',
                                    },
                                    transformation: {
                                        x: 0,
                                        y: 0,
                                        scale: 1,
                                        rotation: 0,
                                    },
                                },
                            },
                        },
                        secondButtonOverlay: {
                            type: 'overlay-component',
                            version: 2,
                            items: [],
                        },
                    },
                    type: 'compare-component',
                    position: slidePosition,
                    version: 1,
                },
                theme: {
                    baseColor: '#FFFFFF',
                    mode: 'light',
                },
                hidden: false,
            };
        }

        return stub;
    }

    toStub () {
        return this._stub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        // if there is no editorPage, this is not a compare
        if (!editorPage) return false;

        return (
            editorPage.templateType === 'Compare' ||
            editorPage.templateId === 'compare'
        );
    }
}

module.exports = CS4Compare;
