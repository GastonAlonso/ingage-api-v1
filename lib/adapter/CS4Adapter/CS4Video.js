import removeWidowWords from '../../removeWidowWords';

import { getSizeFromIOS } from './universal-text';

class CS4Video {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._stub = this.buildStub();
    }

    buildStub () {
        const { _cs4, _editor } = this;

        const { id } = _cs4;
        const editorPage = _editor.pages[id];

        let stub = null;

        if (
            _cs4.overlays[0].properties &&
            _cs4.overlays[0].properties.pageModelData
        ) {
            stub = JSON.parse(_cs4.overlays[0].properties.pageModelData);
        }

        // no stub, build it from content spec
        if (!stub) {
            let video = null;
            let title = null;
            let body = null;
            let titleStyle = null;
            let bodyStyle = null;

            for (const overlay of _cs4.overlays) {
                if (overlay.editorId === 'VideoPageVideo') {
                    video = overlay.video;
                } else if (overlay.editorId === 'VideoPagePreviewContainer') {
                    // look in preview container's overlays
                    for (const previewOverlay of overlay.overlays) {
                        // eslint-disable-next-line default-case
                        switch (previewOverlay.editorId) {
                            case 'VideoPageTitleLabel':
                                title = previewOverlay.text;
                                titleStyle = {
                                    name:
                                        previewOverlay.textStyleId ||
                                        'title-bold',
                                    size: previewOverlay.textSize || '32.0',
                                };
                                break;
                            case 'VideoPageBodyLabel':
                                body = removeWidowWords(previewOverlay.text);
                                bodyStyle = {
                                    name: previewOverlay.textStyleId || 'body',
                                    size: previewOverlay.textSize || '20.0',
                                };
                                break;
                        }
                    }
                }
            }

            stub = {
                type: 'video-page',
                version: 1,
                layout: 'horizontal-tall',
                buttonOverlay: {
                    type: 'overlay-component',
                    version: 1,
                    items: [],
                },
                headingText: {
                    type: 'text-component',
                    version: 2,
                    text: title,
                    fontSize: getSizeFromIOS(titleStyle),
                    fontWeight: 'bold',
                    fontStyle: 'title',
                    textAlignment: 'center',
                    textColor: '#000000',
                },
                bodyText: {
                    type: 'text-component',
                    version: 2,
                    text: body,
                    fontSize: getSizeFromIOS(bodyStyle),
                    fontWeight: 'regular',
                    fontStyle: 'body',
                    textAlignment: 'center',
                    textColor: '#000000',
                },
                video: {
                    type: 'video-component',
                    version: 2,
                    videoFilename: video,
                    backgroundType: {
                        hexColor: '#FFFFFF',
                        style: 'blur',
                    },
                    trimRange: {
                        endTime: 0,
                        startTime: 0,
                    },
                    transformation: {
                        x: 0,
                        y: 0,
                        rotation: 0,
                        scale: 1,
                    },
                    playbackOptions: {
                        playsOnPage: true,
                        hasMuteButton: true,
                        hasFullScreenButton: true,
                    },
                },
                theme: {
                    baseColor: '#ffffff',
                    mode: 'light',
                },
                showHeading: true,
                showBody: true,
            };
        }

        // TODO: Remove when we add custom fonts
        if (_cs4.globalFontOverride) {
            stub.bodyText.fontFamily = _cs4.globalFontOverride;
            stub.headingText.fontFamily = _cs4.globalFontOverride;
        }

        return stub;
    }

    toStub () {
        return this._stub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        // if there is no editorPage, this is not an email
        if (!editorPage) return false;

        return (
            editorPage.templateType === 'Video' ||
            editorPage.templateId === 'video'
        );
    }
}

module.exports = CS4Video;
