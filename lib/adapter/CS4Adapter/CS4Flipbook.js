import removeWidowWords from '../../removeWidowWords';

import { getSizeFromIOS } from './universal-text';

class CS4Flipbook {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._stub = this.buildStub();
    }

    mapLayout (layout) {
        const layoutMapping = {
            'flipbook-full-page': 'full',
            'flipbook-text-horizontal-bottom-third': 'horizontal',
            'horizontal-thirds': 'horizontal',
            'flipbook-text-vertical-right-half': 'vertical-halves',
            'flipbook-text-vertical-right-third': 'vertical-thirds',
            'flipbook-text-squared': 'square',
        };

        return layoutMapping[layout] || 1;
    }

    buildStub () {
        const { _cs4, _editor } = this;

        const { id } = _cs4;
        const editorPage = _editor.pages[id];

        let stub = null;

        if (
            _cs4.overlays[0].properties &&
            _cs4.overlays[0].properties.pageModelData
        ) {
            stub = JSON.parse(_cs4.overlays[0].properties.pageModelData);
        }

        // no stub, build it from content spec
        if (!stub) {
            // TODO: fill in
            const layout = this.mapLayout(editorPage.layoutId);
            const flipbook = _cs4.overlays[2].video;
            const title = _cs4.overlays[0].text;
            const body = removeWidowWords(_cs4.overlays[1].text);

            const titleStyle = {
                name: _cs4.overlays[0].textStyleId || 'title-bold',
                size: _cs4.overlays[0].textSize || '32.0',
            };
            const bodyStyle = {
                name: _cs4.overlays[1].textStyleId || 'body',
                size: _cs4.overlays[1].textSize || '20.0',
            };

            stub = {
                type: 'scrollmotion-page',
                version: 1.0,
                layout,
                buttonOverlay: {
                    type: 'overlay-component',
                    version: 1,
                    items: [],
                },
                headingText: {
                    type: 'text-component',
                    version: 3.0,
                    text: title,
                    fontSize: getSizeFromIOS(titleStyle),
                    fontFamily: _cs4.globalFontOverride || 'benton-sans',
                    fontStyle: 'title',
                    fontWeight: 'bold',
                    textAlignment: 'center',
                    textColor: '#000000',
                },
                bodyText: {
                    type: 'text-component',
                    version: 3.0,
                    text: body,
                    fontSize: getSizeFromIOS(bodyStyle),
                    fontFamily: _cs4.globalFontOverride || 'benton-sans',
                    fontStyle: 'body',
                    fontWeight: 'regular',
                    textAlignment: 'center',
                    textColor: '#000000',
                },
                scrollmotion: {
                    type: 'scrollmotion-component',
                    version: 1.0,
                    videoFilename: flipbook,
                    transformation: {
                        rotation: 0,
                        scale: 1,
                        x: 0,
                        y: 0,
                    },
                    backgroundType: {
                        hexColor: '#FFFFFF',
                        style: 'solid',
                        blurStyle: 'dark',
                    },
                    trimRange: {
                        endTime: 0,
                        startTime: 0,
                    },
                    playbackOptions: {
                        playDirection: 'right',
                        loops: 'false',
                    },
                },
            };
        }

        return stub;
    }

    toStub () {
        return this._stub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        // if there is no editorPage, this is not an email
        if (!editorPage) return false;

        return (
            editorPage.templateType === 'Flipbook' ||
            editorPage.templateId === 'scrollmotion'
        );
    }
}

module.exports = CS4Flipbook;
