import removeWidowWords from '../../removeWidowWords';
import { getSizeFromIOS } from './universal-text';

class CS4Impact {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;
        this._toStub = this.buildStub();
    }

    mapLayout (layout) {
        const layoutMapping = {
            'impact-full-page-text-centered': 'full',
            'impact-text-centered-box': 'snapLarge',
            'impact-text-bottom-left': 'snapLarge',
            'impact-text-top-left': 'snapSmall',
            'impact-text-bottom-right': 'snapSmall',
            'impact-text-vertical-left-half': 'verticalSpreadLarge',
            'impact-full-page-text-left': 'full',
            'impact-text-centered-bar': 'horizontalSpread',
            'impact-text-top': 'horizontalSpread',
            'impact-full-page-text-centered-bold': 'full',
            'impact-full-page': 'hidden',
        };

        return layoutMapping[layout] || 1;
    }

    mapPosition (layout) {
        const positionMapping = {
            'impact-full-page-text-centered': { x: 0, y: 0 },
            'impact-text-centered-box': { x: 0.5, y: 0.5 },
            'impact-text-bottom-left': { x: 0.039, y: 0.8854 },
            'impact-text-top-left': { x: 0.039, y: 0.052 },
            'impact-text-bottom-right': { x: 0.9609, y: 0.8854 },
            'impact-text-vertical-left-half': { x: 0, y: 0 },
            'impact-full-page-text-left': { x: 0, y: 0 },
            'impact-text-centered-bar': { x: 0, y: 0.4 },
            'impact-text-top': { x: 0, y: 0 },
            'impact-full-page-text-centered-bold': { x: 0, y: 0 },
            'impact-full-page': { x: 0, y: 0 },
        };

        return positionMapping[layout] || { x: 0, y: 0 };
    }

    buildStub () {
        const { _cs4, _editor } = this;
        const { id } = _cs4;
        const editorPage = _editor.pages[id];

        let stub = null;

        if (
            _cs4.overlays[0].properties &&
            _cs4.overlays[0].properties.pageModelData
        ) {
            stub = JSON.parse(_cs4.overlays[0].properties.pageModelData);
        }

        // no stub, build it from content spec
        if (!stub) {
            const meta = editorPage.layoutId.split('|');

            const layout = this.mapLayout(meta[0]);
            const position = this.mapPosition(meta[0]);
            const theme = meta[1] || 'light';

            let background = null;
            let backgroundTransformation = null;

            background =
                _cs4.overlays[0].overlays[0].transformedImage ||
                _cs4.overlays[0].overlays[0].image ||
                _cs4.overlays[0].overlays[0].video;

            backgroundTransformation = {
                x: 0,
                y: 0,
                rotation: 0,
                scale: 1,
            };

            let title = null;
            let body = null;

            let titleStyle = null;
            let bodyStyle = null;

            for (const overlay of _cs4.overlays[1].overlays) {
                if (overlay.editorId === 'header') {
                    title = overlay.text;
                    titleStyle = {
                        name: overlay.textStyleId || 'title-bold',
                        size: overlay.textSize || '32.0',
                        align: overlay.textAlign || 'center',
                    };
                } else if (overlay.editorId === 'body') {
                    body = removeWidowWords(overlay.text);
                    bodyStyle = {
                        name: overlay.textStyleId || 'body',
                        size: overlay.textSize || '20.0',
                        align: overlay.textAlign || 'center',
                    };
                }
            }

            const textColor = theme === 'light' ? '#000000' : '#ffffff';

            stub = {
                type: 'impact-page',
                version: 1,
                layout,
                dynamicBox: {
                    type: 'dynamic-box-component',
                    version: 1,
                    blurStyle: theme,
                    ...position,
                },
                buttonOverlay: {
                    type: 'overlay-component',
                    version: 1,
                    items: [],
                },
                subheadingText: {
                    type: 'text-component',
                    version: 1,
                    text: title,
                    fontSize: getSizeFromIOS(titleStyle),
                    fontFamily: 'benton-sans',
                    fontStyle: 'bold',
                    textAlignment: titleStyle.align,
                    textColor,
                },
                bodyText: {
                    type: 'text-component',
                    version: 1,
                    text: body,
                    fontSize: getSizeFromIOS(bodyStyle),
                    fontFamily: 'benton-sans',
                    fontStyle: 'bold',
                    textAlignment: bodyStyle.align,
                    textColor,
                },
                backgroundMedia: {
                    type: 'media-component',
                    version: 1,
                    mediaType: {},
                },
            };

            if (
                _cs4.overlays[0].overlays[0].transformedImage ||
                _cs4.overlays[0].overlays[0].image
            ) {
                stub.backgroundMedia.mediaType.image = {
                    type: 'image-component',
                    version: 2,
                    foregroundBlurStyle: 'light',
                    filename: background,
                    backgroundType: {
                        type: 'media-background-type',
                        version: '1.0',
                        hexColor: '#FFFFFF',
                        style: 'blur',
                        blurStyle: 'dark',
                    },
                    transformation: {
                        x: 0,
                        y: 0,
                        scale: 1,
                        rotation: 0,
                    },
                };
            } else {
                stub.backgroundMedia.mediaType.video = {
                    type: 'video-component',
                    version: 1,
                    foregroundBlurStyle: 'light',
                    videoFilename: background,
                    backgroundType: {
                        type: 'media-background-type',
                        version: '1.0',
                        hexColor: '#FFFFFF',
                        style: 'blur',
                        blurStyle: 'dark',
                    },
                    trimRange: {
                        endTime: 0,
                        startTime: 0,
                    },
                    transformation: {
                        x: 0,
                        y: 0,
                        scale: 1,
                        rotation: 0,
                    },
                    playbackOptions: {
                        playsOnPage: true,
                        hasMuteButton: false,
                        hasFullScreenButton: false,
                    },
                };
            }
        }

        // TODO: Remove when we add custom fonts
        if (_cs4.globalFontOverride) {
            stub.bodyText.fontFamily = _cs4.globalFontOverride;
            stub.subheadingText.fontFamily = _cs4.globalFontOverride;
        }

        return stub;
    }

    toStub () {
        return this._toStub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        if (cs4.overlays) {
            for (const overlay of cs4.overlays) {
                if (
                    overlay.properties &&
                    overlay.properties.pageType === 'impact'
                ) {
                    return true;
                }
            }
        }

        // if there is no editorPage, this is not an email
        if (!editorPage) return false;

        return editorPage.templateType === 'Impact';
    }
}

module.exports = CS4Impact;
