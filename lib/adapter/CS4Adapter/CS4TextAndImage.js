import removeWidowWords from '../../removeWidowWords';

import { getSizeFromIOS } from './universal-text';

class CS4TextAndImage {
    constructor (cs4, editor) {
        this._cs4 = cs4;
        this._editor = editor;

        this._stub = this.buildStub();
    }

    mapLayout (layout) {
        // Map of all possible layouts
        // Note: Does not include no-crop intentionally
        const layoutMapping = {
            'eti-full-page': 'full',
            'full-page-1': 'full',
            'eti-text-horizontal-bottom-third': 'horizontal',
            'horizontal-thirds': 'horizontal',
            'eti-text-vertical-right-half': 'vertical-halves',
            'vertical-halves': 'vertical-halves',
            'eti-text-vertical-right-third': 'vertical-thirds',
            'vertical-thirds': 'vertical-thirds',
            'eti-text-squared': 'square',
        };

        return layoutMapping[layout] || 1;
    }

    mapTheme (theme) {
        // Map of all possible themes
        const themeMapping = {
            // General text-and-image themes
            white: 1,
            'light-gradient': 2,
            'medium-gradient': 3,
            'dark-gradient': 4,

            // Specific no-crop themes
            black_10: 2,
            black_50: 3,
            black_75: 4,
            black_100: 4,
        };

        return themeMapping[theme] || 1;
    }

    buildStub () {
        // Get the content spec and editor spec
        const { _cs4, _editor } = this;

        // Get the single page in the editor that we need
        const { id } = _cs4;
        const editorPage = _editor.pages[id];

        // Get the content for text-and-image pages
        const image = _cs4.overlays[2].image;
        const title = _cs4.overlays[0].text;
        const body = removeWidowWords(_cs4.overlays[1].text);

        const titleStyle = {
            name: _cs4.overlays[0].textStyleId || 'title-bold',
            size: _cs4.overlays[0].textSize || '32.0',
        };
        const bodyStyle = {
            name: _cs4.overlays[1].textStyleId || 'body',
            size: _cs4.overlays[1].textSize || '20.0',
        };

        // Split the layout id on pipes for meta information
        const meta = editorPage.layoutId.split('|');

        // Get the layout and theme from the meta info
        const layout = this.mapLayout(meta[0]);
        const theme = this.mapTheme(meta[1]);

        // It is a no-crop page, use a no-crop stub
        if (meta[0] === 'eti-full-page-photos-horizontal') {
            // Get all the image containers
            const { imageContainers } = editorPage.sections.section1;

            // Take the image containers in the content spec and
            //  map them to usable image objects for our stub.
            const images = imageContainers.map(container => ({
                image: container.overlays[0].image,
                containerHeight: Number(container.height.replace('px', '')),
                containerWidth: Number(container.width.replace('px', '')),
                width: Number(container.overlays[0].width.replace('px', '')),
                height: Number(container.overlays[0].height.replace('px', '')),
                rotation: container.overlays[0].rotation,
                scaleHeight: container.overlays[0].scaleHeight,
                scaleWidth: container.overlays[0].scaleWidth,
                x: Number(container.overlays[0].x.replace('px', '')),
                y: Number(container.overlays[0].y.replace('px', '')),
                xOrigin:
                    Number(container.overlays[0].xOrigin.replace('%', '')) /
                    100,
                yOrigin:
                    Number(container.overlays[0].yOrigin.replace('%', '')) /
                    100,
                xOverlayOffset:
                    Number(
                        container.overlays[0].xOverlayOffset.replace('%', ''),
                    ) / 100,
                yOverlayOffset:
                    Number(
                        container.overlays[0].yOverlayOffset.replace('%', ''),
                    ) / 100,
            }));

            // Get all the text overlays (if any)
            const text = _cs4.overlays.filter(
                overlay => overlay.type === 'label',
            );

            // Return the no-crop stub
            return {
                type: 'no-crop-image-page',
                version: '1.0',
                // Theme configuration
                layout,
                theme,

                // Content
                images,
                text,

                // Report the first image's width and height
                width: images[0].width,
                height: images[0].height,
            };
        }

        let width = _cs4.overlays[2].width.replace('px', '');
        let height = _cs4.overlays[2].height.replace('px', '');

        if (width.includes('%')) {
            // set to native iPad dimensions
            width = 2048;
            height = 1536;
        } else {
            width = parseInt(width, 10);
            height = parseInt(height, 10);
        }

        // It is a normal text-and-image page
        // Return the text-and-image stub
        return {
            type: 'text-and-image-page',
            version: '1.0',
            layout,
            theme,
            image,
            title: {
                content: title,
                style: titleStyle.name,
                size: getSizeFromIOS(titleStyle),
                // TODO: Remove when we add custom fonts
                fontFamily: _cs4.globalFontOverride || 'benton-sans',
            },
            body: {
                content: body,
                style: bodyStyle.name,
                size: getSizeFromIOS(bodyStyle),
                // TODO: Remove when we add custom fonts
                fontFamily: _cs4.globalFontOverride || 'benton-sans',
            },
            _meta: {
                width,
                height,
            },
        };
    }

    toStub () {
        return this._stub;
    }

    static understands (cs4 = { id: '' }, editor = { pages: {} }) {
        const { id } = cs4;
        const editorPage = editor.pages[id];

        // if there is no editorPage, this is not an email
        if (!editorPage) return false;

        return editorPage.templateType === 'ETI';
    }
}

module.exports = CS4TextAndImage;
