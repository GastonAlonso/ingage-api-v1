//@ts-check
const STYLES = {
    ios: {
        header: {
            small: {
                fontSize: '14.0',
            },
            medium: {
                fontSize: '15.0',
            },
            large: {
                fontSize: '16.0',
            },
            // proxy to large since extra large doesn't exist
            get ['extra-large'] () {
                return this.large;
            },
        },
        body: {
            small: {
                fontSize: '17.0',
            },
            medium: {
                fontSize: '20.0',
            },
            large: {
                fontSize: '22.0',
            },
            get ['extra-large'] () {
                return this.large;
            },
        },
        ['title-light']: {
            small: {
                fontSize: '24.0',
            },
            medium: {
                fontSize: '32.0',
            },
            large: {
                fontSize: '40.0',
            },
            ['extra-large']: {
                fontSize: '48.0',
            },
        },
        ['title-bold']: {
            small: {
                fontSize: '24.0',
            },
            medium: {
                fontSize: '32.0',
            },
            large: {
                fontSize: '40.0',
            },
            ['extra-large']: {
                fontSize: '48.0',
            },
        },
    },
    web: {},
};

/**
 * @typedef Styles
 * @type {Object}
 * @property {string} name The name
 * @property {string} size The font size
 */

/**
 * Get the name of the text's size setting
 * @param {Styles} styles
 */
function getSizeFromIOS (styles) {
    if (!styles || !styles.name || !STYLES.ios[styles.name] || !styles.size) {
        return null;
    }

    for (const size in STYLES.ios[styles.name]) {
        if (STYLES.ios[styles.name].hasOwnProperty(size)) {
            if (styles.size === STYLES.ios[styles.name][size].fontSize) {
                return size;
            }
        }
    }

    return null;
}

// @ts-ignore
module.exports = {
    getSizeFromIOS,
};
