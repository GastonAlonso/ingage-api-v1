import chalk from 'chalk'

export const log = x =>
    console.log(
        chalk.blue('[Ingage API][v1] ') + x
    )
