import http from '../lib/http'
import debug from 'debug'
import { Service } from 'leverage-js'

const debugService = debug('[api][v1][service|facebook]')

let url = ''

if (process.env.NODE_ENV === 'development') {
    url = 'https://ssc-dev.scrollmotion.com'
}

else if (process.env.NODE_ENV === 'staging') {
    url = 'https://ssc-qa.scrollmotion.com'
}

else {
    url = 'https://ingage.scrollmotion.com'
}

class S extends Service {
    constructor () {
        super()

        debugService('Creating facebook service...')

        this.config = {
            name: 'facebook'
        }

        debugService('Finished creating facebook service...')
    }

    async scrape (id) {
        try {
            debugService('Sending request to facebook graph api...')

            await http({
                method: 'POST',
                uri: `https://graph.facebook.com`,
                form: {
                    id: `${url}/${id}`,
                    scrape: 'true'
                }
            })

            debugService('Received response from facebook graph api...')
        } catch (error) {
            debugService('Received an error contacting facebook graph api: %o', error)
        }
    }
}

module.exports = new S()
