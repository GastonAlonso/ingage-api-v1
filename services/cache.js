import redis from 'redis'
import debug from 'debug'
import { Service } from 'leverage-js'

const debugService = debug('[api][v1][service|cache]')

class S extends Service {
    constructor () {
        super()

        debugService('Creating caching service...')

        this.config = {
            name: 'cache'
        }

        this.error = null
        this.status = 'disconnected'

        debugService('Creating redis client...')

        this.client = redis.createClient({
            host: process.env.INGAGE_WEB_CACHE,
            port: process.env.INGAGE_WEB_CACHE_PORT,

            db: process.env.INGAGE_WEB_CACHE_DB,
            prefix: process.env.INGAGE_WEB_CACHE_PREFIX
        })

        this.client.on('error', error => {
            debugService('Redis client received an error while connecting:\n%O', error)
            this.status = 'disconnected'
            this.error = error
        })

        this.client.on('connect', () => {
            debugService('Redis client connected successfully.')
            this.status = 'connected'
        })

        this.client.on('reconnecting', () => {
            debugService('Redis client attempting to reconnect.')
            this.status = 'reconnecting'
        })

        debugService('Created redis client.')

        debugService('Finished creating caching service...')
    }

    keys (query) {
        return new Promise((resolve, reject) => {
            if (this.status !== 'connected') {
                debugService('Failure fetching keys, no connection')
                reject({
                    status: this.status,
                    error: 'The Redis server is unavailable'
                })
            }

            else {
                this.client.keys(query, (error, entries) => {
                    if (error) {
                        debugService('Failure fetching keys, error occurred: %o', error)
                        reject({ 
                            status: this.status,
                            error: error
                        })
                    }

                    else {
                        resolve(entries)
                    }
                })
            }
        })
    }

    delete (key) {
        debugService('Deleting key %s...', key)

        return new Promise((resolve, reject) => {
            if (this.status !== 'connected') {
                debugService('Failure deleting key, no connection to a redis store.')
                reject({
                    status: this.status,
                    error: 'The Redis server is unavailable'
                })
            }

            else {
                this.client.del(key, (error, reply) => {
                    debugService('Deleted key %s.', key)
                    resolve(Boolean(reply))
                })
            }
        })
    }

    read (key) {
        debugService('Reading value for key %s...', key)

        return new Promise((resolve, reject) => {
            if (this.status !== 'connected') {
                debugService('Failure reading value for key, no connection to a redis store.')
                reject({
                    status: this.status,
                    error: 'The Redis server is unavailable'
                })
            }

            else {
                this.client.get(key, (error, reply) => {
                    if (error) {
                        debugService('Error getting value for key %s', key)
                        reject(error)
                    }

                    else {
                        debugService('Got value for key %s.', key)
                        resolve(reply)
                    }
                })
            }
        })
    }

    write (key, value) {
        debugService('Writing value for key %s...', key)

        if (this.status !== 'connected') {
            debugService('Failure writing value for key, no connection to a redis store.')

            return Promise.reject({
                status: this.status,
                error: 'The Redis server is unavailable'
            })
        }

        else {
            this.client.set(key, value)

            debugService('Wrote value for key %s.', key)

            // Return a promise so we can chain this method
            return Promise.resolve()
        }
    }
}

module.exports = new S()
