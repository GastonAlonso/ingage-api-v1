FROM node:8.11.1

RUN mkdir -p /opt/app

RUN npm i -g nodemon

ADD ./package.json /tmp/package.json

RUN cd /tmp && \
    yarn --registry https://verdaccio.scrollmotion.com && \
    rm -rf /opt/app/node_modules && \
    cp -a /tmp/node_modules /opt/app/node_modules

ADD . /opt/app/

WORKDIR /opt/app

CMD ["npm", "run", "start"]
