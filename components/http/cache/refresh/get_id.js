import debug from 'debug';
import { Component } from 'leverage-js';

import http from '../../../../lib/http';

const debugComponent = debug('[api][v1][component][http][get@/cache/refresh/:id]');

class C extends Component {
    constructor () {
        super();

        debugComponent('Creating http component...');

        this.config = {
            type: 'http',
            http: {
                path: '/cache/refresh/:id',
                method: 'get',
            },
            dependencies: {
                services: [
                    'cache',
                    'facebook',
                ],
            },
        };

        debugComponent('Finished creating http component...');
    }

    async http (req, res) {
        res.send({ status: 'in progress' });

        try {
            debugComponent('Attempting to remove story %s from cache', req.params.id);

            await Promise.all([
                this.services.cache.delete(`story/mobile/${req.params.id}`),
                this.services.cache.delete(`story/desktop/${req.params.id}`),
                this.services.cache.delete(`story/embed/${req.params.id}`),
            ]);

            debugComponent('Attempting to regenerate story %s', req.params.id);

            await Promise.all([
                http(`http://ingage-server/${req.params.id}?mobile=true`),
                http(`http://ingage-server/${req.params.id}?desktop=true`),
            ]);

            debugComponent('Attempting to re-scrape open graph properties for story %s', req.params.id);

            await this.services.facebook.scrape(req.params.id);
        } catch (error) {
            debugComponent('Received error while trying to remove story %s from cache: %O', req.params.id, error);
        }
    }
}

module.exports = new C();
