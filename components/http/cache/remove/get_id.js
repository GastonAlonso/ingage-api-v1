import debug from 'debug';
import { Component } from 'leverage-js';

const debugComponent = debug('[api][v1][component][http][get@/cache/remove/:id]');

class C extends Component {
    constructor () {
        super();

        debugComponent('Creating http component...');

        this.config = {
            type: 'http',
            http: {
                path: '/cache/remove/:id',
                method: 'get',
            },
            dependencies: {
                services: [
                    'cache',
                ],
            },
        };

        debugComponent('Finished creating http component...');
    }

    async http (req, res) {
        res.json({ status: 'in-progress' });

        try {
            debugComponent('Attempting to remove story %s from cache', req.params.id);

            await this.services.cache.delete(`story/mobile/${req.params.id}`);
            await this.services.cache.delete(`story/desktop/${req.params.id}`);
            await this.services.cache.delete(`story/embed/${req.params.id}`);

            debugComponent('Attempting to re-scrape open graph properties for story %s', req.params.id);

            await this.services.facebook.scrape(req.params.id);
        } catch (error) {
            debugComponent('Received error while trying to remove story %s from cache: %O', req.params.id, error);
        }
    }
}

module.exports = new C();
