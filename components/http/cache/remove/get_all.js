import debug from 'debug';
import { Component } from 'leverage-js';

const debugComponent = debug('[api][v1][component][http][get@/cache/remove/all]');

class C extends Component {
    constructor () {
        super();

        debugComponent('Creating http component...');

        this.config = {
            type: 'http',
            http: {
                path: '/cache/remove/all',
                method: 'get',
            },
            dependencies: {
                services: [
                    'cache',
                ],
            },
        };

        debugComponent('Finished creating http component...');
    }

    async http (req, res) {
        res.json({ status: 'in-progress' });

        try {
            debugComponent('Getting all story', req.params.id);

            const stories = await this.services.cache.keys('story/*');

            for (let story of stories) {
                await this.services.cache.delete(story);
            }
        } catch (error) {
            debugComponent('Received error while trying to remove stories from cache: %O', error);
        }
    }
}

module.exports = new C();
