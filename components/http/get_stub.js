import { Component } from 'leverage-js';

import debug from 'debug';
import http from '../../lib/http';
import flatten from '../../lib/flatten';
import CS4Adapter from '../../lib/adapter/CS4Adapter';

import upgrade from '@scrollmotion/ingage-stubs-upgrader';

const log = debug('[api][v1][component][http][get@/stub/:id]');

import * as video from './updateAssetURLs/pages/video';
import * as impact from './updateAssetURLs/pages/impact';
import * as deck from './updateAssetURLs/pages/deck';
import * as grid from './updateAssetURLs/pages/grid';
import * as compare from './updateAssetURLs/pages/compare';
import * as scrollmotion from './updateAssetURLs/pages/scrollmotion';
import * as gallery from './updateAssetURLs/pages/gallery';

const stubVersionConfig = {
    recursive: true,
    supports: {
        'video-page': 2,
        'impact-page': 3,
        'deck-page': 2,
        'rows-and-columns-page': 2,
        'cover-page': 2,
        'compare-page': 2,
        'scrollmotion-page': 2,
        'gallery-page': 2,

        'button-component': 2,
        'dynamic-box-component': 2,
        'image-component': 4,
        'media-component': 2,
        'media-background-type': 2,
        'media-modal-component': 2,
        'overlay-component': 2,
        'text-component': 2,
        'video-component': 2,
        'compare-component': 1,
        'scrollmotion-component': 1,
    },
};

const updateAssetURLs = {
    'video-page': video,
    'impact-page': impact,
    'cover-page': impact,
    'deck-page': deck,
    'rows-and-columns-page': grid,
    'compare-page': compare,
    'scrollmotion-page': scrollmotion,
    'gallery-page': gallery,
};

let API_URL = 'https://ssc-api-prod.scrollmotion.com';

if (process.env.NODE_ENV === 'staging') {
    API_URL = 'https://ssc-api-qa.scrollmotion.com';
} else if (process.env.NODE_ENV === 'development') {
    API_URL = 'https://ssc-api-dev.scrollmotion.com';
}

class C extends Component {
    constructor () {
        super();

        log('Creating http component...');

        this.config = {
            type: 'http',
            http: {
                path: '/stub/:id',
                method: 'get',
            },
        };

        log('Finished creating http component...');
    }

    async http (req, res) {
        const { id } = req.params;

        try {
            // prettier-ignore
            log(`Getting version for id ${id}`);
            const { version_id: version, content_id: contentId } = await http(
                `${API_URL}/contents/info/${id}`,
            ).then(({ body }) => {
                const json = JSON.parse(body);

                if (!json.version_id) {
                    throw new Error('Story does not exist');
                }

                return json;
            });

            // prettier-ignore
            log(`Getting url to retrieve asset information for id ${id} at version ${version.split('-')[0]}`);
            const {
                created_by: author,
                assets: assetsURL,
                covers,
                thumbnail,
            } = await http(
                `${API_URL}/public_contents/${version}`,
            ).then(({ body }) => JSON.parse(body));

            // Cover for og:image
            // @TODO: Use a "broken cover" asset here as default
            const cover = covers ? covers[0] : thumbnail ? thumbnail : '';

            // get asset information
            // prettier-ignore
            log(`Getting asset information for id ${id} at version ${version.split('-')[0]}`);
            let [
                assets,
                { headers },
            ] = await http(
                `${assetsURL}?order_by=path`,
            ).then(({ body, response }) => [
                JSON.parse(body),
                response,
            ]);

            // calculate pages of assets to request
            const pages = Math.ceil(headers['total-count'] / 20);

            // request more pages if necessary
            if (pages > 1) {
                // prettier-ignore
                log(`Requesting ${pages - 1} more pages for id ${id} at version ${version.split('-')[0]}`);
                const rest = await Promise.all(
                    Array(pages - 1).fill(null).map((_, i) => {
                        log(`Requesting page ${i + 2} for id ${id}`);
                        return http(`${assetsURL}?page=${i + 2}&order_by=path`)
                            .then(({ body }) => JSON.parse(body))
                            .catch(console.error);
                    }),
                );

                assets = flatten(assets.concat(rest));
            }

            // Get asset map
            const assetMap = assets.reduce((map, asset) => {
                map[asset.path] = asset;

                return map;
            }, {});

            let stub;

            // prettier-ignore
            log(`Finding stubs.json for id ${id} at version ${version.split('-')[0]}`);
            // check if stubs.json exists
            const stubs = assets.find(x => x.path === 'stubs.json');

            let stubsURL;
            if (stubs) {
                stubsURL = stubs.download;
            }

            // Story is built on the stubs architecture
            if (stubsURL) {
                // prettier-ignore
                log(`stubs.json for id ${id} found`);
                // prettier-ignore
                log(`Fetching stubs.json for id ${id} at version ${version.split('-')[0]}`);
                stub = await http(stubsURL).then(({ body }) =>
                    JSON.parse(body),
                );

                if (stub.logo && stub.logo.croppedImageFilename) {
                    log(`Patching logo asset...`);
                    const logoAsset = assetMap[stub.logo.croppedImageFilename];
                    const imageAsset = assetMap[stub.logo.imageFilename];

                    stub.logo.croppedImageFilename = logoAsset.download_cf;
                    stub.logo.imageFilename = imageAsset.download_cf;

                    // upgrade logo stub
                    if (stub.logo.logoType === 'CUSTOM_LOGO') {
                        stub.logo.logoType = 'custom';
                    }

                    if (stub.logo.cornerRadiusType) {
                        switch (stub.logo.cornerRadiusType) {
                            case 0:
                                stub.logo.cornerRadiusType = 'circle';
                                break;

                            case 1:
                                stub.logo.cornerRadiusType = 'rounded';
                                break;

                            case 2:
                                stub.logo.cornerRadiusType = 'square';
                                break;
                        }
                    }
                } else {
                    log(`No logo asset...`);
                }

                // Map cover page assets
                if (stub.cover && stub.cover.type in updateAssetURLs) {
                    stub.cover = updateAssetURLs[stub.cover.type].update(
                        stub.cover,
                        assetMap,
                    );
                }

                // Map remaining page assets
                // prettier-ignore
                log(`Patching assets into stub for id ${id}`);
                for (const section of stub.sections) {
                    log(`section: ${section.name}`);
                    for (let page of section.pages) {
                        if (page.type in updateAssetURLs) {
                            log(`Patching ${page.type} assets...`);
                            page = updateAssetURLs[page.type].update(
                                page,
                                assetMap,
                            );
                        } else {
                            // Error - Unsupported Page Type
                            log(`!!Error - unsupported page type ${page.type}`);
                        }

                        let newStub;
                        try {
                            log(`Upgrading ${page.type} stub . . .`);
                            if (page.type === 'rows-and-columns') {
                                page.type = 'rows-and-columns-page';
                            }
                            newStub = upgrade(page, stubVersionConfig);
                            if (newStub.type !== 'error') {
                                page = newStub;
                            } else {
                                log(`upgrade error: ${newStub.message}`);
                            }
                        } catch (e) {
                            // error
                            log(`Upgrade exception: ${e}`);
                        }
                    }
                }
            } else {
                // Legacy Content Spec
                //prettier-ignore
                log(`stubs.json for id ${id} NOT found, using contentspec.json`);
                // prettier-ignore
                log(`Finding contentspec.json for id ${id} at version ${version.split('-')[0]}`);
                const { download: contentspecURL } = assets.find(
                    x => x.path === 'contentspec.json',
                );

                // prettier-ignore
                log(`Finding editor.json for id ${id} at version ${version.split('-')[0]}`);
                const { download: editorURL } = assets.find(
                    x => x.path === 'editor.json',
                );

                // prettier-ignore
                log(`Fetching editor.json and contentspec.json for id ${id} at version ${version.split('-')[0]}`);
                const [
                    editor,
                    contentspec,
                ] = await Promise.all([
                    http(editorURL).then(({ body }) => JSON.parse(body)),
                    http(contentspecURL).then(({ body }) => JSON.parse(body)),
                ]);

                // prettier-ignore
                log(`Building stub for id ${id} at version ${version.split('-')[0]}`);
                try {
                    stub = new CS4Adapter(contentspec, editor).toStub();
                } catch (error) {
                    console.log(
                        `Error building stub for id ${id}: \n${JSON.stringify(
                            error,
                            null,
                            2,
                        )}`,
                    );
                }

                log(`Patching assets into stub for id ${id}`);
                if (stub.cover) {
                    log(`Patching cover page...`);

                    stub.cover = updateAssetURLs[stub.cover.type].update(
                        stub.cover,
                        assetMap,
                    );
                } else {
                    log(`No cover page`);
                }

                if (stub.logo && stub.logo.croppedImageFilename) {
                    log(`Patching logo asset...`);
                    const logoAsset = assetMap[stub.logo.croppedImageFilename];
                    const imageAsset = assetMap[stub.logo.imageFilename];

                    stub.logo.croppedImageFilename = logoAsset.download_cf;
                    stub.logo.imageFilename = imageAsset.download_cf;

                    // upgrade logo stub
                    if (stub.logo.logoType === 'CUSTOM_LOGO') {
                        stub.logo.logoType = 'custom';
                    }

                    if (stub.logo.cornerRadiusType !== null) {
                        switch (stub.logo.cornerRadiusType) {
                            case 0:
                                stub.logo.cornerType = 'circle';
                                break;

                            case 1:
                                stub.logo.cornerType = 'rounded';
                                break;

                            case 2:
                                stub.logo.cornerType = 'square';
                                break;
                        }
                    }
                } else {
                    log(`No logo asset...`);
                }

                log(`Patching sections...`);
                for (const section of stub.sections) {
                    log(`Patching section ${section.name}...`);
                    for (let page of section.pages) {
                        switch (page.type) {
                            case 'no-crop-image-page': {
                                log(`Patching no-crop page...`);

                                for (let image of page.images) {
                                    const asset = assetMap[image.image];

                                    image.image = asset
                                        ? asset.download_cf
                                        : '';
                                }

                                break;
                            }

                            case 'text-and-image-page': {
                                log(`Patching text-and-image page...`);

                                const asset = assetMap[page.image];

                                page.image = asset ? asset.download_cf : '';
                                page._meta.type = asset
                                    ? asset.content_type
                                    : '';

                                break;
                            }

                            case 'compare-page': {
                                log(`Patching compare page...`);

                                page = updateAssetURLs[page.type].update(
                                    page,
                                    assetMap,
                                );

                                break;
                            }

                            case 'gallery-page': {
                                log(`Patching gallery page...`);

                                page = updateAssetURLs[page.type].update(
                                    page,
                                    assetMap,
                                );

                                break;
                            }

                            case 'video-page': {
                                log(`Patching video page...`);

                                page = updateAssetURLs[page.type].update(
                                    page,
                                    assetMap,
                                );

                                break;
                            }

                            case 'scrollmotion-page': {
                                log(`Patching scrollmotion page...`);

                                page = updateAssetURLs[page.type].update(
                                    page,
                                    assetMap,
                                );

                                break;
                            }

                            case 'points-of-interest-page': {
                                log(`Patching points-of-interest page...`);

                                const background = assetMap[page.background];

                                page.background = background
                                    ? background.download_cf
                                    : '';

                                for (const point of page.points) {
                                    switch (point.type) {
                                        case 'image': {
                                            const asset =
                                                assetMap[
                                                    point.properties.image
                                                ];
                                            point.properties.image = asset
                                                ? asset.download_cf
                                                : '';

                                            break;
                                        }
                                        case 'video': {
                                            const asset =
                                                assetMap[
                                                    point.properties.video
                                                ];
                                            point.properties.video = asset
                                                ? asset.download_cf
                                                : '';

                                            break;
                                        }
                                        case 'audio': {
                                            const asset =
                                                assetMap[
                                                    point.properties.audio
                                                ];
                                            point.properties.audio = asset
                                                ? asset.download_cf
                                                : '';

                                            break;
                                        }
                                        default:
                                            break;
                                    }
                                }
                                break;
                            }

                            case 'impact-page': {
                                log(`Patching impact page...`);

                                page = updateAssetURLs[page.type].update(
                                    page,
                                    assetMap,
                                );

                                break;
                            }

                            case 'email-page': {
                                log(`Patching email page...`);

                                const asset = assetMap[page.background];

                                page.background = asset
                                    ? asset.download_cf
                                    : '';

                                break;
                            }

                            case 'deck-page': {
                                log(`Patching deck page...`);

                                // Handle old stub upgrading
                                // ingage-stub-upgrader is not used here because the stub did _not_ receive
                                //      a new version when it was upgraded. Thus, there is no way of knowing...
                                if (page.image) {
                                    page.media = {
                                        type: 'media-component',
                                        version: 1,
                                        mediaType: {
                                            image: {
                                                type: 'image-component',
                                                version: 2,
                                                filename: page.image.filename,
                                                transformation:
                                                    page.image.transformation,
                                                foregroundBlurStyle: 'light',
                                                backgroundType: {
                                                    type:
                                                        'media-background-type',
                                                    version: 2,
                                                    hexColor: '#FFFFFF',
                                                    style: 'blur',
                                                    blurStyle: 'light',
                                                    version: '1.0',
                                                },
                                            },
                                        },
                                    };

                                    // remove unwanted info from stub
                                    delete page.image;
                                }

                                page = updateAssetURLs[page.type].update(
                                    page,
                                    assetMap,
                                );

                                break;
                            }

                            case 'rows-and-columns-page': {
                                log(`Patching rows-and-columns page...`);

                                for (let partition of page.partitions) {
                                    // Handle old stub upgrading
                                    // ingage-stub-upgrader is not used here because the stub did _not_ receive
                                    //      a new version when it was upgraded. Thus, there is no way of knowing...
                                    if (partition.image) {
                                        // Old stub
                                        const asset =
                                            assetMap[partition.image.filename];
                                        const transformation =
                                            partition.image.transformation;

                                        // remove unwanted info from stub
                                        delete partition.image;

                                        partition.media = {
                                            type: 'media-component',
                                            version: 1,
                                            mediaType: {
                                                image: {
                                                    type: 'image-component',
                                                    version: 2,
                                                    filename: asset
                                                        ? asset.download_cf
                                                        : '',
                                                    transformation,
                                                    foregroundBlurStyle:
                                                        'light',
                                                    backgroundType: {
                                                        type:
                                                            'media-background-type',
                                                        version: 2,
                                                        hexColor: '#FFFFFF',
                                                        style: 'blur',
                                                        blurStyle: 'light',
                                                        version: '1.0',
                                                    },
                                                },
                                            },
                                        };
                                    }
                                }

                                page = updateAssetURLs[page.type].update(
                                    page,
                                    assetMap,
                                );

                                break;
                            }

                            default:
                                break;
                        }
                    }
                }
            }

            log(`Stub construction complete for id ${id}`);

            // send back the story information
            res.json({
                stub,
                meta: {
                    id,
                    author,
                    contentId,
                    cover,
                },
            });
        } catch (error) {
            log(`Error during stub creation for id ${id}`);
            res.json({ error });
        }
    }
}

module.exports = new C();
