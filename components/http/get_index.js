import { Component } from 'leverage-js';

class C extends Component {
    constructor () {
        super();

        this.config = {
            type: 'http',
            http: {
                path: '/',
                method: 'get',
            },
        };
    }

    http (req, res) {
        res.json({
            routes: [
                { name: 'get-stub', path: '/stub/:id' },
                { name: 'remove-from-cache', path: '/cache/remove/:id' },
            ],
        });
    }
}

module.exports = new C();
