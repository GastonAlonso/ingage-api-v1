export const update = (audio, assetMap) => {
    if (audio) {
        const asset = assetMap[audio.filename];
        audio.filename = asset ? asset.download_cf : '';
    }

    return audio;
};

export default update;
