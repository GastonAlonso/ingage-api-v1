export const update = (video, assetMap) => {
    if (video) {
        const asset = assetMap[video.videoFilename];

        video.videoFilename = asset ? asset.download_cf : '';
    }

    return video;
};

export default update;
