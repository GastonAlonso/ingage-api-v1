export const update = (image, assetMap) => {
    if (image) {
        const asset = assetMap[image.filename];
        image.filename = asset ? asset.download_cf : '';
    }

    return image;
};

export default update;
