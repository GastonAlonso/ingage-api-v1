import * as media from '../media';
import * as audio from '../audio';

export const update = (button, assetMap) => {
    if (button) {
        if (button.buttonType.media) {
            button.buttonType.media.media = media.update(
                button.buttonType.media.media,
                assetMap,
            );
        }

        if (button.buttonType.audio) {
            button.buttonType.audio = audio.update(
                button.buttonType.audio,
                assetMap,
            );
        }
    }

    return button;
};

export default update;
