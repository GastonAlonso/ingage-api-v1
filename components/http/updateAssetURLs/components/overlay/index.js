import * as button from '../button';

export const update = (overlay, assetMap) => {
    if (overlay) {
        for (let item of overlay.items) {
            item = button.update(item, assetMap);
        }
    }

    return overlay;
};

export default update;
