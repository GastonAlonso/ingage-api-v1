import * as image from '../../components/image';
import * as video from '../../components/video';

export const update = (media, assetMap) => {
    if (media && media.mediaType) {
        if (media.mediaType.image) {
            media.mediaType.image = image.update(
                media.mediaType.image,
                assetMap,
            );
        }

        if (media.mediaType.video) {
            media.mediaType.video = video.update(
                media.mediaType.video,
                assetMap,
            );
        }
    }

    return media;
};

export default update;
