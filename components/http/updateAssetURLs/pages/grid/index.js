import * as media from '../../components/media';
import * as overlay from '../../components/overlay';

export const update = (page, assetMap) => {
    if (page.partitions) {
        for (let index in page.partitions) {
            let partition = page.partitions[index];

            if (partition.media) {
                partition.media = media.update(partition.media, assetMap);
            }

            if (partition.buttonOverlay) {
                partition.buttonOverlay = overlay.update(
                    partition.buttonOverlay,
                    assetMap,
                );
            }

            if (partition.buttonTextOverlay) {
                page.buttonTextOverlay = overlay.update(
                    page.buttonTextOverlay,
                    assetMap,
                );
            }
        }
    }

    return page;
};

export default update;
