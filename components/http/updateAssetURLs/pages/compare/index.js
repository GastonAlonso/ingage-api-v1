import * as media from '../../components/media';
import * as overlay from '../../components/overlay';

export const update = (page, assetMap) => {
    // log(`Patching compare page...`);

    if (page.compare.content.firstMedia) {
        page.compare.content.firstMedia = media.update(
            page.compare.content.firstMedia,
            assetMap,
        );
    }

    if (page.compare.content.secondMedia) {
        page.compare.content.secondMedia = media.update(
            page.compare.content.secondMedia,
            assetMap,
        );
    }

    if (page.compare.content.firstButtonOverlay) {
        page.compare.content.firstButtonOverlay = overlay.update(
            page.compare.content.firstButtonOverlay,
            assetMap,
        );
    }

    if (page.compare.content.secondButtonOverlay) {
        page.compare.content.secondButtonOverlay = overlay.update(
            page.compare.content.secondButtonOverlay,
            assetMap,
        );
    }

    if (page.buttonTextOverlay) {
        page.buttonTextOverlay = overlay.update(
            page.buttonTextOverlay,
            assetMap,
        );
    }

    return page;
};

export default update;
