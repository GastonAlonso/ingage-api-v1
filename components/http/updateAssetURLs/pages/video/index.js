import * as video from '../../components/video';
import * as overlay from '../../components/overlay';

export const update = (page, assetMap) => {
    // log(`Patching deck page...`);

    if (page.video) {
        page.video = video.update(page.video, assetMap);
    }

    if (page.buttonOverlay) {
        page.buttonOverlay = overlay.update(page.buttonOverlay, assetMap);
    }

    if (page.buttonTextOverlay) {
        page.buttonTextOverlay = overlay.update(
            page.buttonTextOverlay,
            assetMap,
        );
    }

    return page;
};

export default update;
