import * as overlay from '../../components/overlay';

export const update = (page, assetMap) => {
    // log(`Patching deck page...`);

    if (page.scrollmotion) {
        const asset = assetMap[page.scrollmotion.videoFilename];

        const scrollmotion = asset
            ? asset.download.replace('/download', '')
            : '';

        page.images = scrollmotion;
    }

    if (page.buttonOverlay) {
        page.buttonOverlay = overlay.update(page.buttonOverlay, assetMap);
    }

    if (page.buttonTextOverlay) {
        page.buttonTextOverlay = overlay.update(
            page.buttonTextOverlay,
            assetMap,
        );
    }

    return page;
};

export default update;
