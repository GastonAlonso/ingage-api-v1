import * as media from '../../components/media';
import * as overlay from '../../components/overlay';

export const update = (page, assetMap) => {
    // log(`Patching deck page...`);

    if (page.backgroundMedia) {
        page.backgroundMedia = media.update(page.backgroundMedia, assetMap);
    }

    if (page.buttonOverlay) {
        page.buttonOverlay = overlay.update(page.buttonOverlay, assetMap);
    }

    if (page.buttonTextOverlay) {
        page.buttonTextOverlay = overlay.update(
            page.buttonTextOverlay,
            assetMap,
        );
    }

    return page;
};

export default update;
