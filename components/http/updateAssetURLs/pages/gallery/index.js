import * as media from '../../components/media';
import * as overlay from '../../components/overlay';

export const update = (page, assetMap) => {
    // log(`Patching gallery page...`);

    for (let item of page.gallery.items) {
        if (item.media) {
            item.media = media.update(item.media, assetMap);
        }

        if (item.buttonOverlay) {
            item.buttonOverlay = overlay.update(item.buttonOverlay, assetMap);
        }
    }

    if (page.buttonTextOverlay) {
        page.buttonTextOverlay = overlay.update(
            page.buttonTextOverlay,
            assetMap,
        );
    }

    return page;
};

export default update;
